import ValueWithValidation from "@/validation/ValueWithValidation"

class Person extends ValueWithValidation {
    constructor(name: string, phone: string) {
        super()
        this.name = name
        this.phone = phone
    }

    name: string
    phone: string

    static empty() {
        return new Person('', '')
    }
}

export default Person
