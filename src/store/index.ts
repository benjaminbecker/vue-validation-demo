import Vue from 'vue'
import Vuex from 'vuex'
import Person from '../domainLogic/Person'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    person: Person.empty(),
  },
  mutations: {
    updatePerson (state, value) {
      const { person } = state
      state.person = {...person, ...value}
    },
  },
  actions: {
  },
  modules: {
  }
})
