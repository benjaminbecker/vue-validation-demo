class ValueWithValidation {
    constructor() {
        this.isValid = true
        this.validationError = {}
    }

    isValid: boolean
    validationError: {[K in Exclude<keyof this, keyof ValueWithValidation>]?: string};
}

export default ValueWithValidation
