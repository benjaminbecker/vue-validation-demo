import Validator from "./Validator";
import Person from '@/domainLogic/Person';

class ValidatorPerson extends Validator<Person> {
    protected validate() {
        const person = this.object
        const { name, phone } = person

        const nameHasMoreThanTwoLetters = () => name.length > 2
        if (!nameHasMoreThanTwoLetters()) {
            this.setError('name', 'Name has to contain more than 2 letters')
        }
        
        const phoneHasLessThanThreeLetters = () => phone.length < 3
        if (phoneHasLessThanThreeLetters()) {
            this.setError('phone', 'Phone wrong')
        }

        return this.object
    }
}

export default ValidatorPerson
