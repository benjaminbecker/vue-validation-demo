import ValueWithValidation from './ValueWithValidation'

abstract class Validator<ObjectClass extends ValueWithValidation> {
    constructor(object: ObjectClass) {
        this._object = object
    }

    get object() {return this._object}

    setError(field: Exclude<keyof ObjectClass, keyof ValueWithValidation>, message: string) {
        this._object.isValid = false
        this._object.validationError[field] = message
    }

    execute() {
        this.clearValidationError()
        return this.validate()
    }

    protected clearValidationError() {
        this._object.isValid = true
        this._object.validationError = {}
    }

    protected abstract validate(): ObjectClass

    protected _object: ObjectClass

}

export default Validator
